# Recommovie

A place for curated movie lists.

## Planned Phases

### 1 - JAMstack: display movie info via React

### 2 - Create a DB for curated lists

### 3 - Add search features (list titles, descriptions, tags)

### 4 - Incorporate Social Media features

### 5 - Create recommendation engine based on user-input list-validation metrics.
