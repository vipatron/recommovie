import React from 'react';
import logo from './logo.svg';
import './App.css';
// import SearchBar from './Search/searchBar';
import output from './Search/IIFE';

function App() {
	let moduleOutput = output;
	console.log('Logged Output: App -> moduleOutput', moduleOutput);
	console.log('moduleOutput.privateVar', moduleOutput.privateVar);

	return (
		<div className='App'>
			New version.
			{/* <SearchBar>default</SearchBar> */}
		</div>
	);
}

export default App;
