import React from 'react';
import Input from './input';

function SearchBar(props) {
	return (
		<div>
			Searchbar goes here, and the children will be on the next line.
			<br />
			<Input> {props.children}</Input>
		</div>
	);
}

export default SearchBar;
