const output = (function() {
	let privateVar = 'secret password';
	console.log('Logged Output: output -> privateVar', privateVar);
	return {
		publicVar: 'you can see me!',
		othervar: 'I am also public!',
		oopsie: privateVar
	};
	// return 3;
})();

// function myTestFunction() {
// 	console.log('called myTestFunction!');
// }
const myTestFunction = function() {
	console.log('called myTestFunction!');
};

myTestFunction();
myTestFunction();

const exampleVar = (3 + 5) / 2;
const example2 = 3 + 5 / 2;
export default output;
