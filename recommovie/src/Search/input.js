import React from 'react';

class Input extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return this.props.children;
	}
}
export default Input;
